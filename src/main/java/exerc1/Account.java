package exerc1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Account {
	
	private String accountNumber;
	
	public Account(String accountNumber) {
		//constructor
		this.accountNumber = accountNumber;
	}
	
	public String getAccountNumber() {
		return accountNumber; //return the account number
	}
	
	public ArrayList getTransaction() throws Exception {
		try {
			List dbTransactionList = Db.getTransactions(accountNumber.trim());
			
			//get the list of transactions
			ArrayList transactionList = new ArrayList();
			
			int i;
			for(i=0; i<dbTransactionList.size(); i++) {
				DbRow dbRow = (DbRow) dbTransactionList.get(i);
				Transaction trans = makeTransactionFromDbRow(dbRow);
				transactionList.add(trans);
			} 
			
			return transactionList;
			
		} catch(SQLException ex) {
			//there was a database error
			throw new Exception("Can't retrieve transactions from database");
			
		}
	}
	
	public Transaction makeTransactionFromDbRow(DbRow row) {
		double currencyAmountInPounds = Double.parseDouble(row.getValueForField("amt"));
		String description = row.getValueForField("desc");
		
		// return the new Transaction object 
		return new Transaction(description, currencyAmountInPounds);
	}
	
	// Override the equals method
	public boolean equals(Account o) { 
		// check account numbers are the same
		return o.getAccountNumber() == getAccountNumber(); 
	}
	

}

class Transaction{

	public Transaction(String description, double currencyAmountInPounds) {
		// TODO Auto-generated constructor stub
	}
	
}

class DbRow{

	public String getValueForField(String string) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

class Db{
	
	public static List getTransactions(String accountNumber) {
		return null;
	}
}
