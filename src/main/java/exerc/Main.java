package exerc;

import static exerc.calculator.beans.TransactionType.REDEMPTION;
import static exerc.calculator.beans.TransactionType.TOP_UP;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import exerc.calculator.FeeCalculator;
import exerc.calculator.FeeCalculatorFactory;
import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.beans.Transaction;
import exerc.calculator.repository.ProductDefinitionStore;
import exerc.calculator.repository.ProductDefinitionStoreService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
	
	private static final Logger log = LogManager.getLogger(Main.class);

	/**
	 * @param args
	 * Main running point, set-up to create the objects and see a run of the small sample
	 */
	public static void main(String[] args) {
		
		List<Transaction> transactions = instatiateTransactions();

		transactions.forEach(Main::executeCalculation);

	}
	
	private static void executeCalculation(Transaction transaction) {
		FeeCalculatorFactory factory = new FeeCalculatorFactory();
		ProductDefinitionStore store = new ProductDefinitionStoreService();
		ProductDefinition productDefinition = store.findProductDefinition(transaction);
		FeeCalculator feeCalculator = factory.createFeeCalculator(productDefinition);
		int fee = feeCalculator.calculateFee(transaction, productDefinition);
		log.info("The following fee was calculated: "+fee);
	}
	
	private static List<Transaction> instatiateTransactions(){
		List<Transaction> transactions = new ArrayList<>();
		Transaction transaction = new Transaction();
		transaction.setForeignExchange(true);
		transaction.setType(REDEMPTION);
		transaction.setTime(LocalDateTime.now());
		transaction.setValue(2000);
		transactions.add(transaction);
		
		transaction = new Transaction();
		transaction.setForeignExchange(true);
		transaction.setTime(LocalDateTime.of(2020, 7, 2, 23, 30));
		transaction.setType(TOP_UP);
		transaction.setValue(6000);
		transactions.add(transaction);
		
		return transactions;
	}
	
	
	
	

}
