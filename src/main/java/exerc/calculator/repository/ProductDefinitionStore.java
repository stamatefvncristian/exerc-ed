package exerc.calculator.repository;

import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.beans.Transaction;

public interface ProductDefinitionStore {
	
	/**
	 * @param transaction
	 * @return
	 * the method o retrieve ProductDefinitions based on transaction as requested in the requirements
	 */
	public ProductDefinition findProductDefinition(Transaction transaction);
	

}
