package exerc.calculator.repository;

import static exerc.calculator.beans.TransactionType.REDEMPTION;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.beans.Transaction;

public class ProductDefinitionStoreService implements ProductDefinitionStore {
	
	private static final Logger log = LogManager.getLogger(ProductDefinitionStoreService.class);

	/* (non-Javadoc)
	 * Dummy implementation of findProductDefinition, just to make the wole project work
	 */
	@Override
	public ProductDefinition findProductDefinition(Transaction transaction) {
		
		
		if(REDEMPTION.equals(transaction.getType())) {
			log.info("The travel product definition has been associated with this transaction");
			return new ProductDefinition("Travel");
		}
		log.info("The Finance product definition has been associated with this transaction");
		return new ProductDefinition("Finance");
	}

}
