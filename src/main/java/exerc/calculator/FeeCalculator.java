package exerc.calculator;

import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.beans.Transaction;

public interface FeeCalculator {
	
	/**
	 * @param transaction
	 * @param productDefinition
	 * @return
	 * method for fee calculation as requested in the requirements
	 */
	public int calculateFee(Transaction transaction, ProductDefinition productDefinition);
}
