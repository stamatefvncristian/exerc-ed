package exerc.calculator;

import static exerc.calculator.beans.TransactionType.REDEMPTION;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.beans.Transaction;

public class ThomasTravelFeeCalculator implements FeeCalculator {
	
	private static final Logger log = LogManager.getLogger(ThomasTravelFeeCalculator.class);

	private static final double FOREIGN_PERCENTAGE = 1.5/100;

	public int calculateFee(Transaction transaction, ProductDefinition productDefinition) {
		log.debug("Starting the calculation of the fee based on ThomasTravelFeeCalculator");
		int fee = 0;
		//calculation of 1.5% fee if foreign exchange is involved
		if(transaction.isForeignExchange()) {
			fee += (int)(transaction.getValue()*FOREIGN_PERCENTAGE);
		}
		// apply a fixed fee of �1 to all redemption transactions
		if(REDEMPTION.equals(transaction.getType())) {
			fee+=1;
		}
		log.debug("Ending the calculation of the fee based on ThomasTravelFeeCalculator");
		return fee;
	}

}
