package exerc.calculator.beans;

import java.time.LocalDateTime;

public class Transaction {
	
	private TransactionType type;
	private int value;
	private LocalDateTime time;
	private boolean foreignExchange;
	
	
	public TransactionType getType() {
		return type;
	}
	public void setType(TransactionType type) {
		this.type = type;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public LocalDateTime getTime() {
		return time;
	}
	public void setTime(LocalDateTime time) {
		this.time = time;
	}
	public boolean isForeignExchange() {
		return foreignExchange;
	}
	public void setForeignExchange(boolean foreignExchange) {
		this.foreignExchange = foreignExchange;
	}


}
