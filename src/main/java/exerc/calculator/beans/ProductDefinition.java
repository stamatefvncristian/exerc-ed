package exerc.calculator.beans;

public class ProductDefinition {
	
	public ProductDefinition(String name) {
		this.name = name;
	}
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
