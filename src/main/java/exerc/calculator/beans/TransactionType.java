package exerc.calculator.beans;

public enum TransactionType {
	
	REDEMPTION, WITHDRAWAL, TOP_UP, BALANCE_ENQUIRY

}
