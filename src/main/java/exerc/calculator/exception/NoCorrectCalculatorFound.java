package exerc.calculator.exception;

public class NoCorrectCalculatorFound extends RuntimeException {

	private static final long serialVersionUID = -2356898811938627351L;

	public NoCorrectCalculatorFound(String message) {
		super(message);
	}

}
