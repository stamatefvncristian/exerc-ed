package exerc.calculator;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.beans.Transaction;

public class TiscoFinanceFeeCalculator implements FeeCalculator{
	
	private static final Logger log = LogManager.getLogger(TiscoFinanceFeeCalculator.class);

	public int calculateFee(Transaction transaction, ProductDefinition productDefinition) {
		log.debug("Starting the calculation of the fee based on TiscoFinanceFeeCalculator");
		
		int fee = 0;
		LocalDateTime transactionTime = transaction.getTime();
		if(transactionTime.getHour()>22) {
			fee += transaction.getValue()*5/100;
		}
		
		log.debug("Ending the calculation of the fee based on TiscoFinanceFeeCalculator");
		return fee;
	}

}
