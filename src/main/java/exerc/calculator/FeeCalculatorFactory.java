package exerc.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import exerc.Main;
import exerc.calculator.beans.ProductDefinition;
import exerc.calculator.exception.NoCorrectCalculatorFound;

public class FeeCalculatorFactory {
	
	private static final Logger log = LogManager.getLogger(Main.class);
	
	/**
	 * @param productDefinition
	 * @return
	 * this method allows the association between product definitions and fee calculators
	 * new product definitions and calculators will have to be registered here
	 */
	public FeeCalculator createFeeCalculator(ProductDefinition productDefinition) {
		if("Travel".equals(productDefinition.getName())) {
			return new ThomasTravelFeeCalculator();
		}else if("Finance".equals(productDefinition.getName())) {
			return new TiscoFinanceFeeCalculator();
		}
		
		log.error("Please check the product definition, no correct fee calculator has been found");
		throw new NoCorrectCalculatorFound("No correct calculator was found for your product definition");
	}

}
